<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@index');

Route::get('services', 'DefaultController@services');

Route::get('portfolio', 'DefaultController@portfolio');

Route::get('web-developement', 'DefaultController@web');

Route::get('web-apps', 'DefaultController@apps');

Route::get('rest-api', 'DefaultController@api');

Route::get('web-sockets', 'DefaultController@sockets');

Route::get('hosting-services', 'DefaultController@hosting');

Route::get('cloud-vps', 'DefaultController@vps');

Route::get('domains', 'DefaultController@domains');

Route::get('cloud-storage', 'DefaultController@cloud');

Route::get('mailboxes', 'DefaultController@mailboxes');

Route::get('ci-cd', 'DefaultController@cicd');

Route::get('git-integration', 'DefaultController@git');

Route::get('automated-testing', 'DefaultController@automated');

Route::get('team', 'DefaultController@team');

Route::get('contact', 'DefaultController@contact');
Route::post('contact', 'DefaultController@contactPost');
