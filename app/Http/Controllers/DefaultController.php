<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DefaultController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function services()
    {
        return view('services.index');
    }

    public function portfolio()
    {
        return view('portfolio.index');
    }

    public function web()
    {
        return view('web.index');
    }

    public function apps()
    {
        return view('web.apps');
    }

    public function api()
    {
        return view('web.api');
    }

    public function sockets()
    {
        return view('web.sockets');
    }

    public function hosting()
    {
        return view('hosting.index');
    }
    public function vps()
    {
        return view('hosting.vps');
    }
    public function domains()
    {
        return view('hosting.domains');
    }

    public function cloud()
    {
        return view('hosting.cloud');
    }

    public function cicd()
    {
        return view('cicd.index');
    }

    public function git()
    {
        return view('cicd.git');
    }

    public function automated()
    {
        return view('cicd.automated');
    }

    public function team()
    {
        return view('team');
    }
    public function contact()
    {
        return view('contact');
    }

    public function contactPost()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'max:30',
            'message' => 'required|min:100'
        ];

        $validData = request()->validate($rules);

        try {

            Mail::to(env('MAIL_CONTACT'))->send(new Contact($validData));

            return ['result' => true];
        } catch (\Exception $ex) {

            return ['result' => false, 'msg' => $ex->getMessage()];
        }
    }
}
