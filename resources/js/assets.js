import { config, library, dom } from '@fortawesome/fontawesome-svg-core'

config.searchPseudoElements = true;

import {
  faSlidersH,
  faUsers,
  faExpandArrowsAlt,
  faTachometerAlt,
  faDatabase,
  faPhoneAlt,
  faUserShield,
  faLanguage,
  faCopyright,
  faPhone,
  faFax,
  faEnvelope,
  faCog,
  faSignOutAlt,
  faExpand,
  faMars,
  faVenus,
  faUserCog,
  faGlobe,
  faCloudUploadAlt,
  faCloudDownloadAlt,
  faTrash,
  faPlus,
  faPencilAlt,
  faAngleUp,
  faAngleDown,
  faSave,
  faUser,
  faHeadset,
  faMapMarked,
  faInbox,
  faQuestionCircle,
  faLeaf,
  faHandshake,
  faLock,
  faMapMarkerAlt,
  faTimesCircle,
  faFileExcel,
  faFilePdf,
  faServer,
  faCode,
  faCodeBranch,
} from '@fortawesome/free-solid-svg-icons'
import {
  faLinkedinIn,
  faLinkedin,
  faTwitter,
  faFacebook,
  faLaravel,

  faGitlab,
  faVuejs,
  faAws,
  faPaypal,
  faSass,
  faDigitalOcean,
  faNodeJs,
  faNode,
  faLinode
} from '@fortawesome/free-brands-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
  faPhone,
  faFax,
  faEnvelope,
  faDatabase,
  faVuejs,
  faPaypal,
  faAws,
  faCog,
  faSass,
  faGitlab,
  faLinkedinIn,
  faLinkedin,
  faSass,
  faDigitalOcean,
  faNode,
  faTwitter,
  faFacebook,
  faLaravel,
  faLinode, faServer, faCode, faCodeBranch)

dom.watch();

// FontAswesomeIcon

Vue.component('icon', FontAwesomeIcon)
