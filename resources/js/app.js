import Vue from 'vue';

window.Vue = Vue;

require('./bootstrap');
require('./assets');


//Shared Vue instance for events
window.Bus = new Vue();

import { Nav, NavItem } from './core/models';
import Form from './core/form';


window.Form = Form;

Vue.component('nav-item', require('./components/navItem.vue').default);


// VIEW COMPONENTS
Vue.component('contact', require('./views/contact.vue').default);


// FORM COMPONENTS
Vue.component('v-input', require('./components/input.vue').default);
Vue.component('v-textarea', require('./components/textarea.vue').default);
Vue.component('typewriter', require('./components/typewriter.vue').default);

window.app = new Vue({
  el: '#app',
  data() {
    return {
      isActive: false,
      canStartVideo: false,
      nav: new Nav([
        new NavItem('Home', '/', false),
        new NavItem('Services', '/services'),
        new NavItem('Portfolio', '/portfolio'),
        new NavItem('Web Developement', '/web-developement', false, [
          new NavItem('Web Apps', null),
          new NavItem('Rest Api', null),
          new NavItem('Web Sockets', null),
        ]),
        new NavItem('Hosting Services', '/hosting-services', false, [
          new NavItem('VPS ', null),
          new NavItem('Domain management', null),
          new NavItem('Cloud Storage', null),
          new NavItem('MailBox Management', null),
        ]),
        new NavItem('CI & CD', '/ci-cd', false, [
          new NavItem('Git Integration ', null),
          new NavItem('Automated Testing & Deployement', null),
        ]),
        // new NavItem('Team', '/team'),
        new NavItem('Contact', '/contact'),
      ])
    }
  },
  mounted() {
    console.log("app")

    let local = this;

    Bus.$on('process-link', navItem => {
      console.log(navItem);
      this.nav.processLink(navItem);

      // Collapsee Menu
      this.isActive = false
    })

    // Set Menu using url
    this.nav.setNavItemByUrl(window.location.pathname)



    var vid = document.getElementById("myVideo");
    vid.oncanplay = function () {
      local.canStartVideo = true;
    };

    let readyInterval = setInterval(() => {


      if (document.readyState === 'complete' && local.canStartVideo) {

        setTimeout(() => {
          // The page is fully loaded
          document.querySelector("#page-loader").setAttribute("class", "page-loader away");

          document.querySelector("#app").removeAttribute("style");
        }, 1000)

        clearInterval(readyInterval)
      }

    }, 1000);





  }
})