window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });


window.log = (...data) => {
  if (process.env.NODE_ENV === 'development') {
    if (data.length > 1)
      console.log(data)
    else
      console.log(data[0])
    return;
  }


  return;
}


/**
 * Toastr Notifications
 */
window.toastr = require('toastr');

toastr.options = {
  debug: false,
  positionClass: 'toast-top-full-width',
  "onclick": function (ev) {

    if (ev.target.hasAttribute('fill'))
      ev.target.parentNode.parentNode.remove()
    else if (ev.target.hasAttribute('data-prefix'))
      ev.target.parentNode.remove()
    else
      ev.target.remove();
  },
  // "fadeIn": 300,
  // "fadeOut": 1000,
  //  "timeOut": 15000,
  //  "extendedTimeOut": 11000
  // hideMethod: function () {
  //   return false
  // }
}

toastr.confirm = (
  message,
  confirmCallBack = '',
  cancelCallBack = 'removeToast(this)',
  btnConfirmText = 'CONFIRM',
  btnCancelText = 'CANCEL'
) => {
  console.log(confirmCallBack)
  const template = `<div>${message}</div>
                <div class="mt-1">
                  <button onclick="${cancelCallBack}" class="button is-dark  is-rounded">${btnCancelText}</button>
                  <button onclick="confirmToast(this,${confirmCallBack})" class="button is-primary  is-rounded">${btnConfirmText}</button>
                </div>`

  const toast = toastr.warning(template, {
    type: 'confirm',
    iconClass: 'iconClass'
  })
  toast.addClass('confirm')
}

toastr.note = (message) => {
  const toast = toastr.success(message, {
    type: 'confirm',
    iconClass: 'iconClass'
  })
  toast.addClass('note')
}

// toastr extended functions to create  confirm toast with buttons

window.removeToast = el => {
  el.parentElement.parentElement.parentElement.remove()
}

window.confirmToast = (el, evenFN) => {
  // eslint-disable-next-line no-eval
  eval(evenFN)
  el.parentElement.parentElement.parentElement.remove()
}



toastr.confirm = (
  message,
  confirmCallBack = '',
  cancelCallBack = 'removeToast(this)',
  btnConfirmText = 'CONFIRM',
  btnCancelText = 'CANCEL'
) => {
  console.log(confirmCallBack)
  const template = `<div>${message}</div>
                <div class="mt-1">
                  <button onclick="${cancelCallBack}" class="button is-dark is-small is-rounded">${btnCancelText}</button>
                  <button onclick="confirmToast(this,${confirmCallBack})" class="button is-primary is-small is-rounded">${btnConfirmText}</button>
                </div>`

  const toast = toastr.warning(template, {
    type: 'confirm',
    iconClass: 'iconClass'
  })
  toast.addClass('confirm')
}

