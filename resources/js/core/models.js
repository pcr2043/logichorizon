

export class NavItem {
  constructor(name = '', url = '', isActive = false, children = [], ajax = true, icon = null) {
    this.name = name;
    this.url = url;
    this.ajax = ajax;
    this.isActive = isActive;
    this.icon = icon;

    this.children = children;
  }
}

export class Nav {

  constructor(items = []) {
    this.items = items;
  }

  processLink(navItem) {

    axios.get(navItem.url).then(response => {

      let DComponent = Vue.extend({
        template: response.data
      });


      let container = document.querySelector('#contents').firstChild;

      let comp = new DComponent();

      comp.$parent = window.App

      comp.$mount(container)

      window.history.pushState(null, navItem.name, navItem.url)

    })
  }

  setNavItemByUrl(url) {
    this.findItem(this.items, url, 0);
  }

  findItem(items, url, index) {

    this.items.forEach((item, index) => {

      if (item.url == url) {
        items[index].isActive = true;
        return;
      }
      else {

        let childrenNavItem = item.children.filter((childItem, index) => {

          // later add check for children to child by calling recursive
          return (childItem.url == url)
        });

        if (childrenNavItem.length > 0) {
          item.isActive = true;
          childrenNavItem[0].isActive = true;
        }
      }
    })
  }
}


export class TypeWriter {
  constructor(node) {
    this.started = false;
    this.completed = false;
    this.node = node;
    this.cssClass = node.getAttribute('class');
    this.text = node.innerText;
    this.textType = ''
  }

  start() {
    let local = this;
    let count = 0;
    local.started = true;
    let intervalType = setInterval(() => {
      if (count < local.text.length) {
        local.textType += local.text[count];
        // local.textType = local.textType.replace("_", '') + '_'
        count++;


      }
      else {
        // local.textType = local.textType.replace("_", '')
        local.completed = true;
        clearInterval(intervalType)

      }

    }, 50)
  }
}