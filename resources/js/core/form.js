
export default class Form {
  constructor(endpoint, model = null) {
    this.endpoint = endpoint
    this.model = model
    this.isSaving = false
    this.errors = [


    ]
  }

  submit(notify = true) {
    this.isSaving = true
    console.log(notify)
    return new Promise((resolve, reject) => {

      this.clearErrors();

      axios[this.verb()](this.buildEndpoint(), this.model)
        .then(response => {

          let data = response.data

          if (data.result && notify)
            toastr.success(data.msg)
          else if (notify)
            toastr.error(data.msg)

          this.isSaving = false
          resolve(response.data)

        })
        .catch(error => {
          window.err = error
          this.isSaving = false
          if (error.response.status === 422) {
            this.setErrors(error.response.data.errors)
            if (notify) window.toastr.error('Please Check the Form')
          } else if (error.response.status === 412 && notify)
            window.toastr.error(error.response.data.msg)
          reject(error)
        })
    })



  }


  create() {

    return new Promise((resolve, reject) => {

      axios.get(this.endpoint + `/create`).then(response => {
        this.model = response.data
        resolve(this.model)
      }).catch(error => {
        //log error
        log(error.response.data)
        //notify
        if (error.response.data['message'] != undefined)
          toastr.error(error.response.data.message)
        reject(error)
      })

    })
  }

  clearErrorByTarget(target) {
    const level0Name = target.name
    const level1Name = target.parentNode.name
    const level2Name = target.parentNode.parentNode.name
    const level3Name = target.parentNode.parentNode.name

    delete this.errors[level0Name]
    delete this.errors[level1Name]
    delete this.errors[level2Name]
    delete this.errors[level3Name]

    this.errors = Object.assign({}, this.errors)
  }

  clearErrors() {
    this.errors = []
  }

  clearError(key) {

    console.log('clearing', key)
    if (this.errors[key] !== undefined) delete this.errors[key]
  }
  getError(key) {

    if (this.errors[key] !== undefined) {
      return this.errors[key][0]
    }

    return '';
  }
  setErrors(errors) {
    this.errors = errors
  }


  verb() {
    return this.model['id'] !== undefined && this.model.id > 0 ? 'put' : 'post'
  }

  buildEndpoint() {

    if (this.model['id'] !== undefined && this.model.id > 0)
      return this.endpoint + '/' + this.model.id
    else
      return this.endpoint

  }
}
