export class Tabs {

  constructor(items = []) {
    this.items = items;


    this.current = this.items.find(item => item.isActive);
    this.index = null;
  }

  setActiveByIndex(index) {

    // hide all tabs
    this.hideTabs();

    // Enable the tab by index
    this.items[index].isActive = true;
    this.current = this.items[index];
    this.index = index;
  }

  hideTabs() {
    this.items.forEach((tab, index) => {
      this.items[index].isActive = false;
    })

    this.current = null;
  }
}

export class Tab {
  constructor(name = '', isActive = false) {
    this.name = name;
    this.isActive = isActive
  }
}