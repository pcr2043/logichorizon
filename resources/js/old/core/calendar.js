

import { Calendar } from '@fullcalendar/core';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import deLocale from '@fullcalendar/core/locales/de';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';


export class CalendarJS {
  constructor(el) {
    this.el = el
    this.calendar = null;
    this.resources = []
    this.events = [
      {
        "id": 1,
        "title": " A Event Text",
        "description": " A Descritipion test Event Text....",
        "status": "er",
        "meta": { "category": "bb", "classification": "ad" },
        "start": moment().format(),
        "end": moment().add(2, 'hours').format(),
        "created_at": "2019-08-27 08:12:52",
        "updated_at": "2019-08-27 08:12:52",
        "selected": false,
        "pivot": {
          "eventable_id": 4, "event_id": 1,
          "eventable_type": "App\\User"
        }
      }];


    this.initialize()


    Bus.$on("set-calendar-events", events => {
      this.calendar.removeAllEvents()
      this.events = events;
      this.events.paginate.data.forEach((event, index) => {
        event.className = "is-primary";
        event.id = event.id.toString();

        this.calendar.addEvent(event);
      });
    });


  }



  initialize() {

    let local = this;

    let event = new Event();
    event.title = "aasda";
    this.calendar = new Calendar(this.el, {
      plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      editable: true,
      locales: [deLocale],
      locale: 'de',
      timeZone: 'UTC+2',
      droppable: true,
      // aspectRatio: 1.5,

      // editable: true,
      // resourceLabelText: 'Rooms',
      // resources: this.resources,
      events: [event],
      eventClick: function (info) {

        console.log(info);
        let event = Object.assign({}, info.event.extendedProps)
        event.id = info.event.id
        event.title = info.event.title
        event.start = event.startFix
        event.end = event.endFix


        Bus.$emit('edit-event', event)
      }

    })

    window.calendar = this;

  }

  render() {

    setTimeout(() => {
      this.calendar.render();
    }, 500)

  }

  show(event) {
    console.log("show ")

  }

}


export class Resource {

  constructor(title = '', eventColor = 'green') {
    this.id = +new Date();
    this.title = title;
    this.eventColor = eventColor
  }
}

export class Event {

  constructor() {
    this.resourceId = null;
    this.start = moment().format();
    this.end = moment().add(2, 'hours').format()
    this.title = null;
    this.description = null;
    this.meta = {
      type: 'event',
      entity: null
    }
    this.status = 0;
    this.entityID = null;
    this.entity = 'users';
    this.users = [];
  }

  attachResource(resource) {
    this.resourceId = resource.id
    this.title = resource.title;
    this.start = moment().format();
    this.end = moment().add(2, 'hours').format()

    return this;
  }
}