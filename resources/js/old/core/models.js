export class Note {
  constructor() {
    this.title = ''
    this.content = ''
    this.category = ''
    this.type = ''
    this.meta = {};
    this.start = null
    this.end = null
    this.duration = false;
    this.status = 0;
    this.created_by = null;
    this.created_at = moment().format();
    this.updated_at = moment().format();
  }
}