

export default class Table {

  constructor(endpoint, filters = {}) {
    this.endpoint = endpoint;
    this.customEndpoint = endpoint;
    this.paginate = {
      total: 0,
      data: []
    }

    this.pageSizes = [12, 50, 100, 200]
    this.checked = false

    // Params Set
    this.params = new Object();

    // Set filters to the params 
    Object.keys(filters).forEach(key => {

      this.params[key] = filters[key]
    });

    this.params.paging = this.pageSizes[0];
    this.params.search = '';
    this.params.sorts = new Object();


    // Loader
    this.isLoading = false;

    // Typing Timeout
    this.isTypingTimeout

    window.table = this;
  }



  getParams() {


    return this.params;
  }
  bind() {

    this.isLoading = true;

    axios.get(this.customEndpoint, { params: this.getParams() }).then(result => {
      this.paginate = result.data
      this.isLoading = false;
    }).catch(error => {
      console.log(error)
      this.isLoading = false;
    })
  }

  bindPromise() {

    return new Promise((resolve, reject) => {
      this.isLoading = true;

      axios.get(this.customEndpoint, { params: this.getParams() }).then(result => {
        this.paginate = result.data
        this.isLoading = false;
        resolve(this.paginate.data)
      }).catch(error => {

        this.isLoading = false;
        resolve([])
      })

    })

  }

  search(value) {

    clearTimeout(this.isTypingTimeout);

    this.isTypingTimeout = setTimeout(() => {

      table.bind();

    }, 1000);
  }


  remove(ids) {

    this.isLoading = true;
    return new Promise((resolve, reject) => {
      axios.delete(this.endpoint + `/${ids}`).then(response => {

        let data = response.data;
        if (data.result)
          toastr.success(data.msg)
        else
          toastr.error(data.msg)
        this.isLoading = false;
        resolve(response.data)

      }).catch(error => {
        //log error
        log(error.response.data)
        //notify
        if (error.response.data['message'] != undefined)
          toastr.error(error.response.data.message)
        this.isLoading = false;
        reject(error)
      })

    })

  }

  unselect() {

    this.checked = false;

    this.paginate.data.forEach((row, index) => {
      this.paginate.data[index].selected = false;
    })
  }
  select(index) {

    this.unselect();

    this.paginate.data[index].selected = true;
  }
  sortBy(key) {

    if (this.params.sorts[key] === undefined)
      this.params.sorts[key] = 'asc'
    else if (this.params.sorts[key] === 'asc')
      this.params.sorts[key] = 'desc'
    else if (this.params.sorts[key] === 'desc')
      delete this.params.sorts[key]


    this.bind();

  }

  getSort(key) {

    if (this.params.sorts[key] === undefined)
      return ''

    return this.params.sorts[key]
  }

  prev() {

    this.customEndpoint = this.paginate.prev_page_url
    if (this.paginate.prev_page_url !== null)
      this.bind();
  }

  next() {

    this.customEndpoint = this.paginate.next_page_url
    if (this.paginate.next_page_url !== null)
      this.bind();
  }

  selectAll(value) {
    this.paginate.data.forEach((row, index) => {
      this.paginate.data[index].selected = value
    });
  }

  setPageSize(size) {
    this.params.paging = size;
    this.bind();
  }
}