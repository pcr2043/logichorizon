// trick to manage backend
// upload files and check on server what is not in the list and remove
// upload what is new

export class File {
  constructor() {
    this.name = ''
    this.extension = ''
    this.type = ''
    // 64bit string for file data
    this.data = ''
    this.path = ''
    this.user_id = ''
    // eslint-disable-next-line no-undef
    this.created_at = moment().format()
  }

  parse(file) {
    this.extension = this.getExtension(file.name).toLowerCase()
    console.log('ext', this.extension)
    this.name = this.getName(file.name) + '.' + this.extension
    this.type = file.type
    this.size = file.size
    this.description = ''

    return this
  }

  getName(name) {
    let reversed = name.split('').reverse().join('');
    return reversed.substring(reversed.indexOf('.') + 1).split('').reverse().join('');

  }

  getExtension(name) {
    let reverseName = name.split('').reverse().join('')
    return reverseName.substring(0, reverseName.indexOf('.')).split('').reverse().join('')
  }
}
