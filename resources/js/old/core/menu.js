
export class Menu {

  constructor(items = [], name = '') {
    this.name = name;
    this.currentItem = null;
    this.lastItem = null;
    this.currentLevel = -1
    this.rendered = true
    this.history = [];
    this.items = items;

    this.path = [];

    if (items.length > 0)
      this.history.push(items);

    // Only for recursive query
    this.parentItems = null;

    window.menu = this;


    Bus.$on('set-menu-path', (item, index) => {
      console.log(item, index)
      if (this.name === 'principal') {
        this.history = this.history.splice(0, index + 1);
        let lastIndex = this.history.length - 1
        this.items = this.history[lastIndex]
        this.path = this.path.slice(0, index + 1);
        this.currentLevel = index;
        this.currentItem = item;


        this.history[this.currentLevel].forEach((citem, index) => {
          this.history[this.currentLevel][index].isActive = false

          if (citem.name === item.name)
            this.history[this.currentLevel][index].isActive = true
        })




        // this.history[this.currentLevel][index].isActive = true

        this.reRender();
      }
    })

  }

  getItems() {
    let maxHistoryLevel = this.history.length;
    this.items = this.history[maxHistoryLevel - 1];

    return this.items;

  }


  getParent() {
    this.rendered = false
    let lastIndex = this.history.length - 1;
    this.history = this.history.slice(0, lastIndex);
    this.items = this.history[lastIndex]

    this.path = this.path.slice(0, lastIndex);

    this.currentLevel--;
    this.reRender();

  }

  reRender() {
    let local = this;
    local.rendered = false;
    setTimeout(() => {
      local.rendered = true;

      if (this.name === 'principal')
        Bus.$emit('menu-path', local.path)

    }, 200)
  }

  resetActive() {

    this.history.forEach((menu, menuIndex) => {
      this.history[menuIndex].forEach((item, index) => {
        this.history[menuIndex][index].isActive = false;
      });
    })
  }
  selectItem(item) {

    let local = this;
    this.rendered = false;

    if (!item.ajax && item.items.length === 0)
      window.location.href = item.url;

    this.resetActive();

    let level = (this.history.length - 1)
    this.history[level][this.history[level].indexOf(item)].isActive = true;

    if (this.history.length == 1)
      this.path = [];

    this.history.push(item.items);



    this.path.push(item)
    this.currentItem = item;


    this.reRender();



  }

  setByRoute(route) {
    if (route !== undefined) {

      //find item on current menu
      let findItem = null;


      for (let i = 0; i < this.items.length; i++) {

        let item = this.items[i]

        if (item.items.length == 0) {
          this.currentLevel = 0;

          this.history = [];
          this.history.push(this.items);

          if (item.url == route) {
            log('found item')
            log(this.history)
            findItem = item;
            this.path = [];
            this.path.push(findItem)
            break;
          }
        }
        else {
          this.currentLevel = 0;
          this.history = [];
          this.path = [];
          this.path.push(item)
          this.history.push(this.items);

          findItem = this.findItemRecursive(item.items, route)

          if (findItem !== null && findItem !== undefined) {
            break;
          }
        }
      }


      // log(this.currentLevel)

      window.foundItem = findItem;

      let index = this.history[this.currentLevel].indexOf(findItem);
      if (this.history[this.currentLevel][index] !== undefined)
        this.history[this.currentLevel][index].isActive = true;
      this.currentItem = this.history[this.currentLevel][index]



      this.reRender()
    }
  }

  findItemRecursive(items, route) {


    let itemFound = null;

    this.history.push(items);
    this.currentLevel++


    for (let i = 0; i < items.length; i++) {

      let item = items[i]

      if (item.items.length == 0) {


        if (item.url.trim() == route.trim()) {
          this.path.push(item)
          itemFound = item

          return itemFound
        }
      }
      else {
        this.path.splice(1)

        this.path.push(item)
        itemFound = this.findItemRecursive(item.items, route)


        if (itemFound !== null) {
          break
        }
        else {
          this.currentLevel--;
          this.history.splice(this.history.length - 1, 1);
        }
      }
    }
    return itemFound
  }


}

export class MenuItem {

  constructor(name = '', url = null, ajax = true, items = []) {
    this.name = name;
    this.url = url;
    this.ajax = ajax;
    this.isActive = false;
    this.items = items;
  }



}