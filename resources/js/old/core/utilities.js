export class Address {
  constructor(address) {
    this.address = address
  }

  format() {
    let strAddress = ''
    // Street
    if (this.address.street.length > 0) strAddress += this.address.street + ' '

    // Number
    if (this.address.number.length > 0 && this.address.street.length > 0)
      strAddress += ', ' + this.address.number + ' '
    else if (this.address.number.length > 0)
      strAddress += this.address.number + ' '

    // Postcode
    if (strAddress.length > 0 && this.address.postcode.length > 0)
      strAddress += ' - ' + this.address.postcode + ' '
    else strAddress += this.address.postcode + ' '

    // Town
    if (this.address.town.length > 0) strAddress += this.address.town

    return strAddress
  }
}

export class DateUtils {
  static format(date) {
    return moment(date).format('MMMM Do YYYY, H:mm')
  }
}


window.randomBetween = function randomBetween(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}