@extends('master')

@section('contents')
<div class="columns portfolio">

  <div class="column">
    <typewriter>
      <p>Our Projects are made with passion combining creativity and perfomance</p>
      <p class="mt-2">This projects are a bit old and some prototypes</p>
      <p class="mt-2">If you are looking to build anwebsite or app, please click contact us:</p>
    </typewriter>
    <p class="mt-3 btn-container"><a type="button" href="/contact"
        class="button is-primary is-small is-rounded has-text-weight-bold">REQUEST
        QUOTE</a>
    </p>
  </div>

  <div class="column">
    <div class="columns is-multiline works">
      <div class="column is-half">
        <img src="../portfolio/edificius.png">
        <a href="http://edificius.ch" target="_blank">EDIFICIUS.CH</a>
        <p>Real State Management - Prototype Demo</p>
      </div>
      <div class="column is-half">
        <img src="../portfolio/lakeviews.png">
        <a href="http://lakeviews.ch" target="_blank">LAKEVIEWS.CH</a>
        <p>Real State Management - Prototype Demo</p>
      </div>
      <div class="column is-half">
        <img src="../portfolio/alltitude.png">
        <a href="http://www.alltitude.com/" target="_blank">ALTITUDE.COM</a>
        <p>IT Consultancy Services </p>
      </div>
    </div>
  </div>

</div>

@endsection