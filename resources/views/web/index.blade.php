@extends('master')

@section('contents')
<div class="columns home">
  <div class="column column-ico">
    {{-- <img src="/services/dev.svg" alt=""> --}}

    <div class="columns tech is-multiline">
      <div class="column is-one-fifth">
        <i class="fab fa-laravel"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-vuejs"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fas fa-database"></i>
      </div>


      <div class="column is-one-fifth">
        <i class="fab fa-paypal"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-sass"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-node"></i>
      </div>



    </div>
  </div>
  <div class="column column-text">

    <typewriter>
      <p>We build software focused on web (Webapps, api's, websockets), combining the latest technologies to archieve
        the best results.</p>
      <p class="mt-3">Our technologies are stable and supported by large communitys , like this we avoid dealing with
        obsolete
        software.</p>
      <p class="mt-2">If you want more to know more about web development services, please send us a message:</p>
    </typewriter>
    <p class="mt-3 btn-container">
      <a type="button" href="/contact" class="button is-primary is-small is-rounded  has-text-weight-bold">I WANT TO
        KNOW MORE</a>
    </p>

  </div>

</div>

@endsection