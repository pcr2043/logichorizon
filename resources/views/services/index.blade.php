@extends('master')

@section('contents')

<div class="columns is-multiline services">
  <div class="column is-offset-one-third is-two-thirds text">
    <typewriter>
      <p>We Build Software for the Web and we care about the process</p>
      <p class=" mt-2">Also some mobile development, desktop apps etc... </p>
      <p class=" mt-2">We Evaluate things like Eficiency , Cost & Creativity...</p>
    </typewriter>
    <p class="mb-2 mt-2 btn-container">
      <a type="button" href="/contact" class="button is-primary is-small is-rounded  has-text-weight-bold">MORE INFO</a>
    </p>

    <div class="columns image-columns">
      <div class="column is-one-quarter-widescreen">
        <img src="../services/web.svg">
      </div>
      <div class="column is-one-quarter-widescreen">
        <img src="../services/performance.png">
      </div>
      <div class="column  is-one-quarter-widescreen">
        <img src="../services/creativity.png">
      </div>
    </div>


  </div>
</div>


@endsection