@extends('master')

@section('contents')
<div class="columns home">
  <div class="column column-ico">
    {{-- <img src="/services/dev.svg" alt=""> --}}

    <div class="columns tech is-multiline">



      <div class="column is-one-fifth">
        <i class="fas fa-server"></i>
      </div>

      <div class="column is-one-fifth">
        <i class="fab fa-digital-ocean"></i>
      </div>

      <div class="column is-one-fifth">
        <i class="fab fa-linode"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-aws"></i>
      </div>



    </div>
  </div>
  <div class="column column-text">

    <typewriter>
      <p> We used Digital Ocean and Linode for our Apps on the Cloud cause they are stable allowing flexibility &
        scalability. We think in long term.</p>
      <p class="mt-2">Also we use Amazon AWS Services to supports our apps with services like: S3 Storage..</p>
      <p class="mt-2">Finally we can setup your domain and mailboxes by looking the best option in market that feets
        best your needs..</p>
    </typewriter>
    <p class="mt-3 btn-container">
      <a type="button" href="/contact" class="button is-primary is-small is-rounded  has-text-weight-bold">I WANT TO
        KNOW MORE</a>
    </p>

  </div>

</div>

@endsection