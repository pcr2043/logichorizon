@component('mail::message')
<h3>Received a contact from {{ $data['name'] }}</h3>

<table>
  <tbody>
    <tr>
      <td>Name</td>
      <td>{{ $data['name']}}</td>
    </tr>
    <tr>
      <td>Email</td>
      <td>{{ $data['email']}}</td>
    </tr>
    @if(isset($data['mobile']))
    <tr>
      <td>Mobile</td>
      <td>{{ $data['mobile']}}</td>
    </tr>
    @endif
    <tr>
      <td>Message</td>
      <td>{{ $data['message']}}</td>
    </tr>
  </tbody>
</table>

Thanks,<br>
{{ config('app.name') }}
@endcomponent