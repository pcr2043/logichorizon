@extends('master')

@section('contents')
<div class="columns is-multiline is-mobile home">
  <div class="column is-full-mobile is-half-widescreen column-ico">
    {{-- <img src="/services/dev.svg" alt=""> --}}

    <div class="columns is-mobile tech is-multiline">
      <div class="column is-one-fifth">
        <i class="fab fa-laravel"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-vuejs"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fas fa-database"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-aws"></i>
      </div>

      <div class="column is-one-fifth">
        <i class="fab fa-paypal"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-sass"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-node"></i>
      </div>

      <div class="column is-one-fifth">
        <i class="fab fa-digital-ocean"></i>
      </div>
      <div class="column is-one-fifth">
        <i class="fab fa-gitlab"></i>
      </div>
    </div>
  </div>
  <div class="column is-full-mobile  is-half-widescreen">

    <typewriter>
      <p class="mb-3">Logic Horizon is a Prototype Startup and was born in 2019 as result of many years of freelance
        working experience in IT.</p>
      <p class="inline">The main goal of the company is start growing delevering HQ Software, mainly focused on
      </p>
      <p class="font-700 inline has-text-primary"> Web Apps and Mobile Applications.</p>

      <p class="mt-3">Each project is a result of a thorough analysis, taking each phase of a project into a global
        holistic view as result all pieces of the software are combined in perfect harmony`.</p>
    </typewriter>
  </div>
</div>

@endsection