@extends('master')

@section('contents')
<div class="columns home">
  <div class="column column-ico">
    {{-- <img src="/services/dev.svg" alt=""> --}}

    <div class="columns tech is-multiline">


      <div class="column is-one-fifth">
        <i class="fas fa-code"></i>
      </div>

      <div class="column is-one-fifth">
        <i class="fas fa-code-branch"></i>
      </div>


      <div class="column is-one-fifth">
        <i class="fab fa-gitlab"></i>
      </div>

      <div class="column is-one-fifth">
        <i class="fas fa-server"></i>
      </div>

      <div class="column is-one-fifth">
        <i class="fab fa-digital-ocean"></i>
      </div>

      <div class="column is-one-fifth">
        <i class="fab fa-linode"></i>
      </div>
    </div>
  </div>
  <div class="column">
    <typewriter>
      <p>We use Gitlab to push our code and we use the CI & CD features to test and deploy our
        code...</p>
      <p class="mt-3">Gitlab Runners aree installed in our machines to providee fast develery code by
        make using of 'Hot Deployements', for development and production environments</p>
    </typewriter>
    <p class="mt-3 btn-container">
      <a type="button" href="/contact" class="button is-primary is-small is-rounded  has-text-weight-bold">CONTACT
        US</a>
    </p>
  </div>
</div>

@endsection