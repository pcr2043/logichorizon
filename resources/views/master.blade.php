@if(request()->ajax() === false)
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <title>LOGIC HORIZON - IT REMOTE WORK </title>
    <link rel="stylesheet" href="{{ mix('css/app.css')}}">
</head>



<body>

    <div id="page-loader" class="page-loader">
        <img src="/logo.png" class="logo" alt="logic horizon">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>

    <section id="app" style="height:100%;" class="hero app is-fullheight">
        <div class="video-wrapper">
            <div class="overlay"></div>
            <video id="myVideo" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                <source src="movie.mov" type="video/mp4">
            </video>
        </div>

        <div class="hero-header">
            <img src="/logo.png" class="logo" alt="logic horizon">
            <button class="hamburger hamburger--elastic" :class="{'is-active': isActive}" @click="isActive= !isActive"
                type="button" aria-label="Menu" aria-controls="navigation">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
            {{-- <div class="logo-text">
                <h3 class="has-text-primary is-size-2 has-text-weight-bold has-text-right">LOGIC HORIZON</h3>
                <div class="has-text-white  has-text-weight-bold has-text-right">SOFTWARE ENGINNERING</div>
            </div> --}}
        </div>
        <div class="hero-body">

            <div class="columns is-multiline is-mobile main">
                <div class="column is-full-desktop is-one-third-widescreen column-menu" :class="{'show' :  isActive }">
                    <div class="container">
                        <aside class="menu">
                            <ul class="menu-list">
                                <nav-item :parent="nav.items" :nav-item="item" v-for="(item, index) in nav.items"
                                    :key="index">
                                </nav-item>
                            </ul>
                        </aside>
                    </div>

                </div>

                <div v-show="!isActive" class="column is-full-desktop is-two-thirds-widescreen">
                    @endif
                    <div id="contents" class="container">
                        @yield('contents')
                    </div>
                </div>
                @if(request()->ajax() === false)
            </div>
        </div>

        <div class="hero-footer">
            <div class="columns">
                <div class="column contacts">
                    <a class="mr-1" href="tel:+351964103627"><span
                            class="fas has-text-primary fa-phone mr-6px"></span>+351 964 103
                        627</a>
                    <a href="mailto:patrick.indigo.horizon@gmail.com "><span
                            class="fas has-text-info  fa-envelope  mr-6px"></span>patrick.indigo.horizon@gmail.com</a>
                </div>
                <div class="column quote">
                    Simplicity is an acquired taste. - Katharine Gerould
                </div>
                <div class="column social-and-tools">
                    <a href="https://twitter.com/patrick_twitts"><span class="fab fa-twitter"></span></a>
                    <a href="https://www.facebook.com/patrick.c.reis.9"><span class="fab fa-facebook"></span></a>
                    <a href="https://www.linkedin.com/in/patrick-cabral-a376a9172/"><span
                            class="fab fa-linkedin"></span></a>
                    <a href="https://gitlab.com/pcr2043/"><span class="fab fa-gitlab"></span></a>

                </div>
            </div>
        </div>
    </section>
</body>
<script src="{{ mix('js/app.js') }}"></script>

</html>
@endif