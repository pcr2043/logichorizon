@extends('master')

@section('contents')
<contact inline-template>
  <div class="columns contact-page">
    <div class="column column-ico">


      <div class="columns is-multiline portugal">
        <div class="column">
          <img src="/images/portugal.png" alt="">
        </div>
        <div class="column address">
          <h5 class="company">LOGIC HORIZON</h5>
          <p>Rua dos Emigrantes, n.7</p>
          <p>6270 - 351 </p>
          <p>Sazes da Beira - Seia - PT</p>
          <br>
          <p>NATIONAL PARK - Serra da Estrela</p>

          <a class="mt-2" href="tel:+351964103627"><span class="fas has-text-primary fa-phone mr-6px"></span>+351 964
            103
            627</a>
          <a class="mt-05" href="mailto:patrick.indigo.horizon@gmail.com "><span
              class="fas has-text-info  fa-envelope  mr-6px"></span>patrick.indigo.horizon@gmail.com</a>
        </div>


      </div>
    </div>
    <div class="column column-form">


      <form @submit.prevent="submit">
        <div class="columns is-multiline">
          <div class="column is-full">
            <v-input label="Name" v-model="form.model.name" placeholder="Enter you Name" :error="form.getError('name')">
            </v-input>
          </div>
          <div class="column is-full">
            <v-input label="Email" v-model="form.model.email" placeholder="Enter you Email"
              :error="form.getError('email')">
            </v-input>
          </div>
          <div class="column is-full">
            <v-input label="Mobile" v-model="form.model.mobile" placeholder="Enter you Mobile"
              :error="form.getError('mobile')">
            </v-input>
          </div>
          <div class="column is-full">
            <v-textarea label="Message" v-model="form.model.message" placeholder="Enter you Message"
              :error="form.getError('message')">
            </v-textarea>
          </div>
          <div class="column is-full">
            <button type="submit" class="button is-small is-primary is-rounded is-pulled-right">
              <span class="button is-loading" v-if="form.isSaving"></span>
              <div :class="{'text' : form.isSaving }">SEND</div>
            </button>
          </div>
        </div>


      </form>

    </div>

  </div>
</contact>

@endsection